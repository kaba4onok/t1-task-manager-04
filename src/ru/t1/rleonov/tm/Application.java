package ru.t1.rleonov.tm;

import static ru.t1.rleonov.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        final String arg = args[0];
        switch (arg) {
            case CMD_ABOUT:
                showAbout();
                break;
            case CMD_VERSION:
                showVersion();
                break;
            case CMD_HELP:
                showHelp();
                break;
            default:
                showErrorArg();
        }
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Task Manager");
        System.out.println("Developer: Roman Leonov");
        System.out.println("E-mail: rleonov@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.1");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show application version.\n", CMD_VERSION);
        System.out.printf("%s - Show application info.\n", CMD_ABOUT);
        System.out.printf("%s - Show application commands.\n", CMD_HELP);
    }

    public static void showErrorArg() {
        System.err.println("Error! This argument is not supported.");
    }

}
