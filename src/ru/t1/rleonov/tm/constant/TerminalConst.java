package ru.t1.rleonov.tm.constant;

public class TerminalConst {

    public final static String CMD_VERSION = "version";
    public final static String CMD_ABOUT = "about";
    public final static String CMD_HELP = "help";

}
