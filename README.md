# TASK-MANAGER

## DEVELOPER

**NAME**: Roman Leonov

**E-MAIL**: rleonov@t1-consulting.ru

**E-MAIL**: kaba4onok@gmail.com

**SKYPE**: rom.leonov

## SOFTWARE

**JAVA**: JDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: i5

**RAM**: 16 Gb

**SSD**: 256 Gb

## APPLICATION RUN

```
java -jar ./task-manager.jar
```
